<?php
namespace App;
use DB;
use App\Http\Controllers\Controller;
class listnews extends Controller {

	public static function listnews(){
	 $response= DB::table('news as n')
	         ->join('img as i', 'i.news_id', '=', 'n.news_id')
	         ->join('category as c', 'c.category_id', '=', 'n.category_id')
	        ->orderBy('n.news_id', 'desc')

           ->paginate(10);
		   return $response;
		  
	}



	public static function news_data1($id="",$limit){
	



	$news=DB::table('news as n')
	         ->join('img as i', 'i.news_id', '=', 'n.news_id')
	         ->join('category as c', 'c.category_id', '=', 'n.category_id')
	        ->orderBy('n.news_id', 'desc')
	 	    ->where('n.category_id',$id)
	        ->orderBy('n.news_id', 'desc')
	        ->paginate($limit);
           
		   return $news;
		 
	}
	

		public static function news_post($limit){
	 


	  
		$news =	DB::table('news as n')
	         ->join('img as i', 'i.news_id', '=', 'n.news_id')
	         ->join('category as c', 'c.category_id', '=', 'n.category_id')
	        ->orderBy('n.news_id', 'desc')
	 
	  
	 ->paginate($limit);
           
		   return $news;
		   
	}
public static function priority($priority){
	 

	 $news =DB::table('news as n')
	         ->join('img as i', 'i.news_id', '=', 'n.news_id')
	         ->join('category as c', 'c.category_id', '=', 'n.category_id')
	        -> where('n.priority',$priority)
	        ->orderBy('n.news_id', 'desc')
	 	 ->paginate(5);
           
		   return $news;
		   
	}

	public static function listprofile(){
	 $data= DB::table('profile as p')
	         ->join('file as f', 'f.profile_id', '=', 'p.profile_id')
	            ->orderBy('p.profile_id', 'desc')

           ->paginate(10);
		   return $data;
		   
	}
		public static function mycity(){
	 $mycity= DB::table('mycity as n')
	         ->join('img as i', 'i.mycity_id', '=', 'n.mycity_id')
	         ->join('place as c', 'c.id', '=', 'n.city_id')
	         ->join('shop_category as s', 's.shop_id', '=', 'n.shop_id')
	        ->orderBy('n.mycity_id', 'desc')

           ->paginate(10);

		   return $mycity;
		   
	}
	
	public static function mycity1($id){

	 $mycity= DB::table('mycity as n')
	         ->join('img as i', 'i.mycity_id', '=', 'n.mycity_id')
	         ->join('place as c', 'c.id', '=', 'n.city_id')
	         ->join('shop_category as s', 's.shop_id', '=', 'n.shop_id')

	         ->where('city_id',$id)
	         
	        ->orderBy('n.mycity_id', 'desc')

           ->paginate(10);

		   return $mycity;
		   
	}

	public static function shop($shop){

	 $mycity= DB::table('mycity as n')
	         ->join('img as i', 'i.mycity_id', '=', 'n.mycity_id')
	         ->join('place as c', 'c.id', '=', 'n.city_id')
	         ->join('shop_category as s', 's.shop_id', '=', 'n.shop_id')

	         ->where('n.shop_id',$shop)
	         
	        ->orderBy('n.mycity_id', 'desc')

           ->paginate(10);

		   return $mycity;
		   
	}

}
?>