<?php 
namespace App\Http\Controllers\Web\Backend;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Product;
use App\listnews;
use App\listjob;
use App\mycity;
USE App\Place;
USE App\shop_category;
use Auth;
use DB;
use Validator;
use Image;

class Admin_Pannel extends Controller {
	 	
	// public function __construct()
	// {
	// 	$this->middleware('auth');
	// }

	public function shop_func()
	{

		
	return view('Backend.news.shop');

     
	}
	public function add_shop(Request $request) {
$city=new shop_category;
$city->shop_category=$request->city;
$city->save();

return response()->json(array(
                    'success' => true,
                    'message' => "insert success."
                    ));

}
	public function displayshop_func(){

	$city=shop_category::paginate(500);

	return $city;
}

public function add_city(Request $request) {
$city=new Place;
$city->city=$request->city;
$city->save();

return response()->json(array(
                    'success' => true,
                    'message' => "insert success."
                    ));

}

public function displaycity(){
	$city=Place::paginate(500);
	return $city;
}

	public function news_mycity(Request $request) {
        
        $rules     = array(
            'phone' => 'required|max:255',
            'name' => 'required',
            'city_id' => 'required','category_id' => 'required',
            'product_image' => 'image|mimes:jpeg,png,jpg|max:2048');
            

               $validator = Validator::make($request->all(), $rules);

        $data = $request->all();

        if ($validator->fails()) {
            return response()->json(array(
                'success' => false,
                'message' => $validator->getMessageBag()->toArray()
            ));
        }



          $mycity=new mycity;
         
                $mycity->name= $request->name;
             	$mycity->mobile=$request->phone;
             	$mycity->land_line=$request->landline;
             	$mycity->email=$request->email;
             	$mycity->address=$request->address;
             	$mycity->city_id=$request->city_id;
             	$mycity->shop_id=$request->category_id;
             	
             
             	
     		$mycity->save();

            $lastId =$mycity->id;
          
      
         
      $img = $request->file('product_image');
        if($img!=null) {   
            
        
      $id=$mycity->mycity_id;
   
        $img_cat="img"; 

        $i_year=date('Y'); $i_month=date('m'); $image_name=uniqid(); 
        $ext=$img->getClientOriginalExtension(); 
        if (!is_dir("img" . $img_cat)) {
            mkdir("img" . $img_cat);
        }
        
        if (!is_dir("img" . $img_cat . "/" . $i_year)) {
            mkdir( "img" . $img_cat . "/" . $i_year);
        }
        if (!is_dir("img" . $img_cat . "/" . $i_year . "/" . $i_month)) {
            mkdir("img" . $img_cat . "/" . $i_year . "/" . $i_month);
        }
        list($width, $height) = getimagesize($img);

                 $ratio = $width/$height; // width/height

                 if ($ratio > 1) {
                 $width = 150;
                 $height = 150/$ratio;
                 } else {
                 $width = 150*$ratio;
                 $height = 150;
                 }

       Image::make($img)->resize($width, $height)->save('img'.$img_cat.'/'.$i_year.'/'.$i_month.'/'. $image_name.'_s.'.$ext  );


    

                 if ($ratio > 1) {
                 $width = 600;
                 $height = 600/$ratio;
                 } else {
                 $width = 600*$ratio;
                 $height = 600;
                 }

       Image::make($img)->resize($width, $height)->save('img'.$img_cat.'/'.$i_year.'/'.$i_month.'/'. $image_name.'_l.'.$ext  );

     $img=DB::insert('insert into img(mycity_id,category,year,month,image_name,ext) values(?,?,?,?,?,?)',array($lastId,$img_cat,$i_year,$i_month,$image_name,$ext));  

 }


                    
                    return response()->json(array(
                    'success' => true,
                    'message' => "insert success."
                    ));
             
                
           
      
         
    }

	public function news_international()
	{
		
return view('Backend.news.news_international' )->with(array('data'=>listnews::listnews()));
     
	}
	public function news_job()
	{
		
 return view('Backend.news.news_jobpost' )->with(array('data'=>listjob::listjob()));
     
	}

	public function news_city()
	{
		
$data =  DB::table('city')
		 ->paginate(3);
		
		return view('Backend.news.city' , ['data' => $data]);
     
	}

	public function news_state()
	{
		
	
		return view('Backend.news.news_state' )
		->with(array('data'=>listnews::listnews()));
     
	}

	public function settings()
	{
		return view('Backend.news.accountsetting' )->with(array('data'=>listnews::listnews()));
	}
	
	public function news_local()
	{
		

		return view('Backend.news.news_local' )->with(array('data'=>listnews::listnews()));
     
	}


	public function mycity()
	{

		
	return view('Backend.news.mycity')->with(array("mycity"=>listnews::mycity()));

     
	}

	public function news_national()
	{
		

		return view('Backend.news.news_national' )->with(array('data'=>listnews::listnews()));
     
	}



public function news_complient()
	{
		 $data =  DB::table('complient')
		 ->paginate(10);
		
		return view('Backend.news.complient' , ['data' => $data]);
		  
	}


public function complient()
	{
		 $response = DB::table('complient')
		 ->paginate(10);
		 
		 
		
		  return $response;
		   
	}

		public function news_profile()
	{
		
		return view('Backend.news.profile' )->with(array('data'=>listnews::listprofile()));

			
     
	}
	public function profile()
	{
		return listnews::listprofile();
	}

	public function news_fetch()
	{
		return listnews::listnews();
	}


// job post display
	public function news_fetch1()
	{
		return listjob::listjob();
	}


public function mycity_fetch()
	{
		return listnews::mycity();
	}


	public function news_displayjob()
	{
		return view('Backend.news.news_job' )->with(array('data'=>listjob::listjob()));
	}

//end
	


	
	
	 public function getLogout()
	{	
	  Auth::logout();
	  return redirect('/superadmin');
	}

	

}
