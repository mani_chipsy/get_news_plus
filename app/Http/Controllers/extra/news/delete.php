<?php 
namespace App\Http\Controllers\news;
use App\Http\Controllers\Controller;
use  DB;
class Delete extends Controller {
 	public function delete_func($id) { 
	    
		$res=DB::table('news')
	        ->where('news_id',$id) ->delete();

	        
		$imgpath= DB::table('img')->where('news_id',$id)->get();
		 $image_path1=public_path("img".$imgpath[0]->category."/".$imgpath[0]->year."/".$imgpath[0]->month."/".$imgpath[0]->image_name."_l.".$imgpath[0]->ext);
         $image_path2=public_path("img".$imgpath[0]->category."/".$imgpath[0]->year."/".$imgpath[0]->month."/".$imgpath[0]->image_name."_s.".$imgpath[0]->ext);
// $image_path="";
//echo $image_path1;

 if (File_exists($image_path1)) {
        //File::delete($image_path);
        unlink($image_path1);
         unlink($image_path2);
    }
		$res=DB::table('img')
	         ->where('news_id',$id) ->delete();	
		if($res==1) {
		return response()->json(array(
						'success' => true,
						'message' => "News details successfully deleted."
						));
	   }else{
	    return response()->json(array(
						'success' => false,
						'message' => "Some thing went wrong."
						));
	   }
	}





	public function deletejob_func($id) { 
	    
		$res=DB::table('jobpost')
	        ->where('news_id',$id) ->delete();

	        
		if($res==1) {
		return response()->json(array(
						'success' => true,
						'message' => "job post details successfully deleted."
						));
	   }else{
	    return response()->json(array(
						'success' => false,
						'message' => "Some thing went wrong."
						));
	   }
	}

}
