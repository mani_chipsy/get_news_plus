<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class News extends Model
{
    public $table = 'news';
    public $fillable = ['news_id','category_id','title','description'];
    
   public function news_data()
	{

		return $this->belongsTo('App\img', 'news_id', 'news_id')
		
		
		->orderBy('news_id', 'DESC');

	}
	public  function news_data1()
	{

		return $this->belongsTo('App\category', 'category_id', 'category_id');

		
		
}
	
}



