   <header class="panel-heading bg-light">
                                    <ul class="nav nav-tabs nav-justified menu">

 
 <li class=" {{ Request::is('superadmin/news/international') ? 'active' : '' }}" >
 <a href="{{ url('superadmin/news/international') }}"   ><b>International</b></a></li>
<li class=" {{ Request::is('superadmin/news/national') ? 'active' : '' }}"  >
<a href="{{ url('superadmin/news/national') }}"><b>National News</b></a></li>
<li class="{{ Request::is('superadmin/news/state') ? 'active' : '' }}" >
<a href="{{ url('superadmin/news/state') }}" ><b>State News</b></a></li>
<li class="{{ Request::is('superadmin/news/local') ? 'active' : '' }}">
<a href="{{ url('superadmin/news/local') }}" ><b>Local News</b></a></li>

                                    </ul>
            
         </header>