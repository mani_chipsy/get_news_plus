<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>NEWS Portal</title>
   <link rel="apple-touch-icon" sizes="180x180" href="/favicons/apple-touch-icon.png">
<link rel="icon" type="image/png" sizes="32x32" href="/favicons/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="16x16" href="/favicons/favicon-16x16.png">
<link rel="manifest" href="/favicons/manifest.json">
<link rel="mask-icon" href="/favicons/safari-pinned-tab.svg" color="#5bbad5">
<meta name="theme-color" content="#ffffff">
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="/bootstrap/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css" -->
  <!-- Ionicons -->
  <!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css"> -->
   <link href="/Frontend/font-awesome/css/font-awesome.min.css" rel="stylesheet">
 <link rel="stylesheet" href="/css/jquery-confirm.min.css">

<link rel="stylesheet" href="/css/app.v1.css" type="text/css" />
      <link rel="stylesheet" href="/css/datepicker.css" type="text/css" />
      <link rel="stylesheet" href="/css/slider.css" type="text/css" />
      <link rel="stylesheet" href="/css/chosen.css" type="text/css" />
      <!-- <link rel="stylesheet" href="/css/jquery.bootstrap-touchspin.css" type="text/css" /> -->
      <link rel="stylesheet" href="/css/typehead.css" type="text/css" />
      
      
         <!-- <link rel="stylesheet" href="https://netdna.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css"> -->

</head>