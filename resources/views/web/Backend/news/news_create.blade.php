@extends('layouts.master_layout')
@section('content')
<section class="panel panel-default">
                                 <header class="panel-heading bg-light">
                                    <ul class="nav nav-tabs nav-justified">

 
 <li class="active" ><a href="#home"  data-toggle="tab">International</a></li>
<li><a href="#profile" data-toggle="tab">National News</a></li>
<li><a href="#messages" data-toggle="tab">State News</a></li>
<li><a href="#settings" data-toggle="tab">Local News</a></li>
                                    </ul>
            
         </header>


<div class="panel-body">
                                    <div class="tab-content">

 <div class="tab-pane active news-list"  id="home">

 
                                 
                              
                                      
                           
 <section id="up" class="panel panel-default">
                           <header class="panel-heading font-bold "> International News </header>
                           <div class="panel-body cat" id="100" cat="100">
                             
                              <form class="form-horizontal" id="300" name="form-insert" >
                                  <div class="form-group">
                                    <label class="col-sm-2 control-label">Titile</label> 
                                    <div class="col-sm-10"> <input type="text" id="title" name="title" class="form-control" placeholder="Enter Title " required="true"> </div>
                                 </div>

                                  <div class="line line-dashed b-b line-lg pull-in"> </div>
                                 <div class="form-group">
                                    <label class="col-sm-2 control-label">Descriptions</label> 
                                   <div class="col-sm-10"> 
                                   <textarea id="description" name="description" rows="7" class="form-control ckeditor" placeholder="Write your message.." required="true"></textarea>

                                 </div>
                                 </div>



                            <div class="line line-dashed b-b line-lg pull-in"> </div>

                     <button type="button" id="show" style='visibility:hidden;'  >
                     <b style="color: #187bbb;font-size: 15px;padding-left: 210px;">Do you Want to change the Image Click Here</b> </button>
                                <!-- <button id="hide">Hide</button> -->
                          <div class="form-group img">
                                    <label class="col-sm-2 control-label">Image</label> 
                                    <div class="col-sm-10"> 
                                    

                                     <input type="file" class="form-control" name="product_image" id="product_image"  placeholder="Select Log"  required="true" >
                                 </div>
                                 </div>

         <div class="col-md-2 col-md-offset-2">      

                                     
<button type="button" name="btninsert" id="btninsert" val="200"  class="btn btn-primary btn-block btn-flat" style='visibility:show'  >Insert</button>
<button type="button" name="btnupdate" id="btnupdate"  class="btn btn-primary btn-block btn-flat" value=" " style='visibility:hidden;    margin-top: -35px;' >Update</button>

</div>
</form>
</div>
</section>
<section id="content">
                  <section class="vbox">
                     <section class="scrollable padder">
                      <!--   <div class="m-b-md">
                           <h3 class="m-b-none">Datatable</h3>
                        </div> -->
                        <section class="panel panel-default">
                           <header class="panel-heading"> News <i class="fa fa-info-sign text-muted" data-toggle="tooltip" data-placement="bottom" data-title="ajax to load the data."></i> </header>
                           <div class="table-responsive">
                              <table class="table table-striped m-b-none  table-hover" ">
                                 <thead>
                                    <tr>
                                    <th width="5%">ID</th>
                                    <th width="15%">Title</th>
                                     <th width="25%">Description</th>
                                    <th width="10%">Image</th>
                                    <th width="5%">Action</th>
                                                                           </tr>
                                 </thead>
                                 <tbody class="company-list"> 
                <?php $i=1; ?>
                 @foreach($data as $news)
                 <tr id="{{$news->news_id}}">
                  <td width="5%">{{$i++}}</td>
                  
                  <td width="15%">{{$news->title}}</td>
                  <td width="25%">{{$news->description}}</td>
                  <td width="10%">{{$news->image_name}}.{{$news->ext}}</td>
                 
 


<td width="5%"><div class="btn-group ">
                  <button type="button" class="btn btn-default">Action</button>
                  <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="true">
                    <span class="caret"></span>
                    <span class="sr-only">Toggle Dropdown</span>
                  </button>
                  <ul class="dropdown-menu action_control" role="menu">
                    <li id="101"><a href="#up">Edit</a></li>
                    <li id="103"><a href="javascript:void(0)">Delete</a></li>
                 </ul>
                </div></td>
                </tr>
                @endforeach()
                @if ($i==1)  <tr><td colspan="5" style="color:red">No Record Found.</td>
                </tr>
                @endif
                </tbody>
                              </table>
                               <div class="box-footer clearfix companypage">
              <ul id="pagination-demo" class="pagination-lg pagination pagination-sm no-margin pull-left">
               <li class="prv_page  "><a href="javascript:void(0)" >Previous</a></li>
               <?php $i = 1;  $page=ceil($data->total()/10);?>
                             @for ($i = 1; $i <= $page; $i++)
                               <li  class="page_number @if($i==1)  active @endif" id="{{ $i}}"><a href="javascript:void(0)" >{{ $i}}</a></li>
                             @endfor   
                               <li class="nxt_page"><a href="javascript:void(0)" >Next</a></li>
              </ul>
            </div>
                           </div>
                        </section>
                     </section>
                  </section>
                  <a href="#" class="hide nav-off-screen-block" data-toggle="class:nav-off-screen,open" data-target="#nav,html"></a> 
               </section>

            
<!-- <section>

                                 

                                 <div class="box-body no-padding">
              
              <table class="table table-hover">
             <thead><tr>
                  <th>ID</th>
                  <th>Title</th>
                   <th>Description</th>
                  <th>Image</th>
                  <th>Action</th>
                 
                </tr>
                </thead>


                <tbody class="company-list"> 
                <?php $i=1; ?>
                 @foreach($data as $news)
                 <tr id="{{$news->news_id}}">
                  <td>{{$i++}}</td>
                  
                  <td>{{$news->title}}</td>
                  <td>{{$news->description}}</td>
                  <td>{{$news->image_name}}.{{$news->ext}}</td>
                 
 


<td><div class="btn-group ">
                  <button type="button" class="btn btn-default">Action</button>
                  <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="true">
                    <span class="caret"></span>
                    <span class="sr-only">Toggle Dropdown</span>
                  </button>
                  <ul class="dropdown-menu action_control" role="menu">
                    <li id="101"><a href="#up">Edit</a></li>
                    <li id="103"><a href="javascript:void(0)">Delete</a></li>
                 </ul>
                </div></td>
                </tr>
                @endforeach()
                @if ($i==1)  <tr><td colspan="5" style="color:red">No Record Found.</td>
                </tr>
                @endif
                </tbody>
                
                </table>
                
               <div class="box-footer clearfix companypage">
              <ul id="pagination-demo" class="pagination-lg pagination pagination-sm no-margin pull-left">
               <li class="prv_page  "><a href="javascript:void(0)" >Previous</a></li>
               <?php $i = 1;  $page=ceil($data->total()/10);?>
                             @for ($i = 1; $i <= $page; $i++)
                               <li  class="page_number @if($i==1)  active @endif" id="{{ $i}}"><a href="javascript:void(0)" >{{ $i}}</a></li>
                             @endfor   
                               <li class="nxt_page"><a href="javascript:void(0)" >Next</a></li>
              </ul>
            </div>

                </div>
                </section> -->
                
     <!--  <div class="modal fade" id="modal-form">
         <div class="modal-dialog">
            <div class="modal-content">
               <div class="modal-body wrapper-lg">

                     <div class="col-sm-12">
<section class="panel panel-default">
                           <header class="panel-heading font-bold "> International News </header>
                           <div class="panel-body cat" id="100" cat="100">
                             
                              <form class="form-horizontal" name="form-insert" >
                                  <div class="form-group">
                                    <label class="col-sm-2 control-label">Titile</label> 
                                    <div class="col-sm-10"> <input type="text" id="title" name="title" class="form-control" placeholder="Enter Title "> </div>
                                 </div>

                                  <div class="line line-dashed b-b line-lg pull-in"> </div>
                                 <div class="form-group">
                                    <label class="col-sm-2 control-label">Descriptions</label> 
                                   <div class="col-sm-10"> <textarea id="description" name="description" rows="7" class="form-control ckeditor" placeholder="Write your message.."></textarea>
                                 </div>
                                 </div>



                            <div class="line line-dashed b-b line-lg pull-in"> </div>
                                
                          <div class="form-group">
                                    <label class="col-sm-2 control-label">Image</label> 
                                    <div class="col-sm-10"> 
                                    

                                     <input type="file" class="form-control" name="product_image" id="product_image"  placeholder="Select Log"  required="true" >
                                 </div>
                                 </div>

         <div class="col-md-2 col-md-offset-2">                     
<button type="button" name="btninsert"  class="btn btn-primary btn-block btn-flat" >Insert</button>

                                  </form>
                                  </div>
                                  </section>
                                  </div> -->

                                
                                  


</div>
                  

     



                                       <div class="tab-pane " id="profile">
                                                                             
                           
 <section class="panel panel-default">
                           <header class="panel-heading  font-bold ">National News </header>
                           <div class="panel-body cat " id="101" cat="101">
                             
                              <form class="form-horizontal" name="form-insert" >
                                  <div class="form-group">
                                    <label class="col-sm-2 control-label">Titile</label> 
                                    <div class="col-sm-10"> <input type="text" id="title" name="title" class="form-control" placeholder="Enter Title "> </div>
                                 </div>

                                  <div class="line line-dashed b-b line-lg pull-in"> </div>
                                 <div class="form-group">
                                    <label class="col-sm-2 control-label">Descriptions</label> 
                                   <div class="col-sm-10"> <textarea id="description" name="description" rows="7" class="form-control ckeditor" placeholder="Write your message.."></textarea>
                                 </div>
                                 </div>



                            <div class="line line-dashed b-b line-lg pull-in"> </div>
                                
                          <div class="form-group">
                                    <label class="col-sm-2 control-label">Image</label> 
                                    <div class="col-sm-10"> 
                                    

                                     <input type="file" class="form-control" name="product_image" id="product_image"  placeholder="Select Log"  required="true" >
                                 </div>
                                 </div>

         <div class="col-md-2 col-md-offset-2">                     
<button type="button" name="btninsert"  id="201" class="btn btn-primary btn-block btn-flat " >Insert</button>

                                 </form>
                                 </div>
                                 </section>
                                       </div>
                                       <div class="tab-pane" id="messages">message</div>
                                       <div class="tab-pane" id="settings">settings</div>
                                    </div>
                                 </div>
</section>
       <!-- </div>
      
    </div> -->
    
    @endsection