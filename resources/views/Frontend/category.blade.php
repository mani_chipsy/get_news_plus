@extends('Frontend.layouts.master_layout')
@section('content')
    <style>
.column-post  img{
    max-width: 100%;
    height: 250px;
    object-fit: cover;
}
</style>
    <!--breadcrumb-->
   <section class="breadcrumb">
   <div class="container">
      <ol class="cm-breadcrumb">
         <li><a href="/">Home</a></li>
         <li><a href="#">News</a></li>
           @foreach ($data->slice(0, 1) as $news)
             <li class="cm-active">
            {{$news->category_name}}
         </li>
      
          @endforeach

      </ol>
   </div>
</section>

    <!--end breadcrumb-->
    
    <!--content-->
    <section class="content item-list">
        <div class="container">
            <div class="row">
                <div class="col-md-9 page-content-column">

<div class="col-md-12">

 <div class="breaking-news-slide clearfix">
                          <h5 class="bg1">Breaking News</h5>
                          <div class="newsslider">
                              <ul class="slides">
                               @foreach ($data->slice(0, 4) as $news)
                                <?php

                        $title=preg_replace('/[ ,]+/', '-', trim("{$news->title}"));
                        $category_name=preg_replace('/[ ,]+/', '-', trim("{$news->category_name}"));

                        ?>
                                  <li> <a href="/single_page/{{$category_name}}/{{$news->news_id}}/{{$title}}">
                                  <?php 
                                   
                                    echo str_limit("{$news->title}", $limit = 50, $end = '...')
                                   
                                  
                                   ?>

                                  </a> </li>

                                  @endforeach
                                  
                                 <!--  <li> <a href="#">The World’s Youngest Billionaire is Just 24...</a> </li>
                                  <li> <a href="#">Stewart had a really good pattern for a handbag Just 24….</a> </li> -->
                              </ul>
                          </div>
                      </div>
                      <!--breaking slide end-->


                       
                     <div class="news-carousel wow animated fadeInUp" data-wow-delay="0.2s">
                        @foreach ($data->slice(0, 1) as $news)
                      
                          @if($news->category_id==103)
                          <h3 class="main-title">    

                                    <label class="col-sm-3 control-label" style="margin-top: 7px;">Select City</label>
                                

                      <select id="selectid" name="city" style="width: 150px;"  class="form-control mySelect " onchange="getval(this);">
                               
                           <option value='0' selected>Select City</option>
                         @foreach(App\city::get() as $city)
                          <!-- $selected = '';
                          if($city->id == 1)    // Any Id
                          {
                              $selected = 'selected="selected"';
                          } -->

                          <option class="state"   value='{{ $city->id }}' > {{ $city->city}}</option>

                          @endforeach
                          </select>
                          @else
                            <h3 class="main-title" style="height: 37px;"> 
                             <div class="col-md-4">   
                              {{$news->category_name}}
                              </div>
                         </h3>
                        @endif

       
         </h3>
                           @endforeach


                        <div id="{{$news->category_id}}" class=" company-list ">
                          @foreach ($data  as $news)
                          <div class="col-md-4  ">
                              <div class="column-post news-card ">
                          
                          

                           
                                 <a href="#" class="img-thumbnail">
                                 <img src="/img{{$news->category }}/{{$news->year }}/{{$news->month }}/{{$news->image_name }}_l.{{$news->ext }}" class=" thumbnail"  alt="Image" >
                                  </a>
                                 <div class="topic ">
                              <?php

                        $title=preg_replace('/[ ,]+/', '-', trim("{$news->title}"));
                        $category_name=preg_replace('/[ ,]+/', '-', trim("{$news->category_name}"));

                        ?>
                          <!--  <a href="category/{{$category_name}}/{{$news->category_id}}/{{$title}}" class="tag bg1">{{$news->category_name}}</a> -->

              <h4 class="media-heading"><a href="/single_page/
              {{$category_name}}/{{$news->news_id}}/{{$title}}">

                                 <?php 
                                    
                                    echo str_limit("{$news->title}", $limit = 20, $end = '...')
                                                                    
                                   ?>
                                   </a></h4>
                                 <!--    <h4><a href="#"</a></h4> -->
                                    <p class="news-description" align="justify">       <?php 
                                   
                                
                                    $s= strip_tags("{$news->description}");
                                    echo str_limit($s, $limit = 75, $end = '...');
                                   
                                  
                                   ?> </p>
                                    <ul class="post-tools">
                                       <li> by <a href=""><strong> GetNewsPlus</strong> </a></li>
                                       <li>  {{App\TimeControl::time_ago($news->created_at)}} </li>
                                      
                                    </ul>
                                 </div>
                              </div>
                             
                           </div>
                            @endforeach
                            </div>
                            </div>
                            <div >
                            <br>
                           <!--  {{ $data->links() }} -->

                            </div>
                           <!--item-->

                         
                          
                       <!--  </div> -->
                       
                        <!--post slider-->
                    

</div>
<div class="box-footer clearfix companypage"><!-- pagination-demo -->
              <ul id="pagination-demo" class="pagination-lg pagination pagination-sm no-margin pull-left">
          
              </ul>
            </div>    




              
              </div>      
           
       
    
         @include('Frontend.sidebar.sidebar')
              
           </div>
                 
                     
       </div>

</section>
<script src="/plugins/jQuery/jquery-2.2.3.min.js"></script>
 <script src="/js/jquery.twbsPagination.js"></script>
<script type="text/javascript">

var page = "<?php $i = 1;  $page=ceil($data->total()/15); echo $page; ?>";

  var cat = $('div.company-list').attr('id');
 // alert(cat);

            $('#pagination-demo').twbsPagination({
                totalPages: page,
                visiblePages: 6,
                onPageClick: function(event, page) {
                 
 // alert("hii");
                       $.job.display(cat,page);
                    // if (!$('page').last().hasClass('active')) {
                    //     var nxt = $('.pagination').find('li.active').find('a').html();
                    //     nxt++;
                    //     $('.pagination').find('li.active').removeClass('active');
                    //     $('.pagination').find('li.page_number#' + nxt).addClass('active');
                      
                    // }
                }
            });
//

     
//    $('#pagination-demo').twbsPagination({

//                 totalPages: 2,
//                 visiblePages: 6,
//                 onPageClick: function(event, page) {

//                        $.complient.display(page);
                    // if (!$('page').last().hasClass('active')) {
                    //     var nxt = $('.pagination').find('li.active').find('a').html();
                    //     nxt++;
                    //     $('.pagination').find('li.active').removeClass('active');
                    //     $('.pagination').find('li.page_number#' + nxt).addClass('active');
                      
                    // }
            //     }
            // });
</script>
    <!-- content end-->

   @endsection
