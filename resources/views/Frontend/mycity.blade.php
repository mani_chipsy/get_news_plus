@extends('Frontend.layouts.master_layout')
@section('content')
     
        <link href="/Frontend/css/card.css" rel="stylesheet">
         <script src="{{asset('Assets/angular/angular.js')}}"></script>
    <script src="{{asset('Assets/angular/route.js')}}"></script>
    <script src="{{asset('Assets/angular/ui-bootstrap-tpls-2.5.0.min.js')}}"></script>
    <!--breadcrumb-->
   <section class="breadcrumb">
   <div class="container">
      <ol class="cm-breadcrumb">
         <li><a href="/">Home</a></li>
         <li><a href="#">What is in Your City</a></li>
         
      

      </ol>
   </div>
</section>

    <!--end breadcrumb-->
    
    <!--content-->
    <section class="content item-list" ng-controller="NewsController">
        <div class="container">
            <div class="row">
                <div class="col-md-9 page-content-column">

<div class="col-md-12">



                       
                     <div class="news-carousel wow animated fadeInUp" data-wow-delay="0.2s">
                        
                          <div class="main-title col-md-12 " style="height:fit-content;">    
                          <div class="col-sm-6">

                                    <label class="col-sm-5 control-label" style="margin-top: 7px;">Select City</label>
                                

                      <select id="selectid1" name="city" style="width: 150px;"  class="form-control mySelect " >
                               
                           <option value='0' selected>Select City</option>
                          @foreach(App\Place::get() as $city)
                          <!-- $selected = '';
                          if($city->id == 1)    // Any Id
                          {
                              $selected = 'selected="selected"';
                          } -->

                          <option class="state"   value='{{ $city->id }}' > {{ $city->city}}</option>

                          @endforeach

                          </select>
                         </div>

                         <div class="col-sm-6">
                                    <label class="col-sm-5 control-label" style="margin-top: 7px;">Select Shop</label>
                                

                      <select id="selectid" name="cat" style="width: 150px;"  class="form-control mySelect " >
                               
                           <option value='0' selected>Select Shop</option>
                         @foreach(App\shop_category::get() as $city)
                          <!-- $selected = '';
                          if($city->id == 1)    // Any Id
                          {
                              $selected = 'selected="selected"';
                          } -->

                          <option class="state"   value='{{ $city->shop_id }}' > {{ $city->shop_category}}</option>

                          @endforeach
                          </select>
                          </div>



       
       </div>


                          

                     
                       <!--  </div> -->
                       
                        <!--post slider-->
                    

</div>
<div class="company-list" ng-repeat="mycity in visitors" ng-cloak>
     
         
            <div class="col-sm-6 col-md-6 col-lg-6 mt-4">
                <div class="card">
                  <div class="col-sm-3 col-md-3 col-lg-3 " style="    width: 37%;">
                  <div ng-if='mycity.category=="null"'>
                       <img src="/images/noimage.png" class=" thumbnail  "  alt="Image" >
                       </div>
                 <img src="/img<% mycity.category %>/<% mycity.year %>/<% mycity.month %>/<% mycity.image_name %>_l.<% mycity.ext %>" class=" thumbnail  "  alt="Image" >
                    </div>
                       <div class="col-sm-4 col-md-4 col-lg-4 mt-2">
                    <div class="card-block" style="    width: 230px;">
                        <h6 class="card-title" title="<% mycity.name %>" class="tooltip" style="font-size: 13px;width: 200px;
        text-overflow: ellipsis;
overflow: hidden;
white-space: nowrap;"> <% mycity.name %></h6>


                        <div class="meta">
                            <a href=""><br> <% mycity.shop_category%></a>
                        </div>
                        <div class="card-text">
                           <?php

                        $title=preg_replace('/[ ,]+/', '-',trim("<%mycity.name%>"));
                  


                        ?>
                        <a href="single_page_city/<% mycity.mycity_id %>/{{$title}}" ><br>  Click Here to More Info</a>
                          <!-- <i class="fa fa-mobile" aria-hidden="true" style="    margin-right: 14px;"></i><% mycity.mobile %><br>
                          <i class="fa fa-phone" aria-hidden="true" style="margin-right: 10px"></i><% mycity.land_line %><br>
                          <i class="fa fa-envelope" aria-hidden="true"></i><% mycity.email %><br> -->
                     
                        </div>
                    </div>
                    </div>
                  <div class="col-sm-6 col-md-6 col-lg-6 mt-4" style="width: 100%;">
                    <!-- <div class="card-footer"> -->
                      <ul class="fa-ul" style="    
    padding-top: 4px;">
<!--   <li><i class="fa-li fa fa-location-arrow"></i><span style="    font-size: 10px;"><% mycity.address %></span></li> -->
 
</ul>
                   <!--  </div> -->
                </div>
            </div>
            </div>

             

            </div>
        
      
  





              
              </div>   
<div ng-if='visitors.length =="0"' ng-cloak>
                                             <p  align="center" style="color: red"><b>No records to display</b></p>
                                          </div>
                  <div class="col-md-6 ">
              <ul uib-pagination total-items="totalVisitors" ng-model="currentPage"  name="pagination" class="pagination-sm" boundary-links="true" rotate="false" max-size="maxSize" items-per-page="visitorsPerPage"></ul>
                                       </div>   
          <!--  <div class="box-footer clearfix companypage">
              <ul id="pagination-demo" name="pagination" class="pagination-lg pagination pagination-sm no-margin pull-left">
          
              </ul>
            </div>  
        -->
    </div>
         @include('Frontend.sidebar.sidebar')
              
           </div>
                 
                     
       </div>

</section>
<script src="/plugins/jQuery/jquery-2.2.3.min.js"></script>
 <script src="/js/jquery.twbsPagination.js"></script>

  
  <script src="/Frontend/js/city.js"></script>
  
   @endsection
