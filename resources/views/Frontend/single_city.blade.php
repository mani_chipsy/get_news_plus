@extends('Frontend.layouts.master_layout')
@section('content')
<!--breadcrumb-->
<link href="/Frontend/css/card.css" rel="stylesheet">
         <script src="{{asset('Assets/angular/angular.js')}}"></script>
    <script src="{{asset('Assets/angular/route.js')}}"></script>
    <script src="{{asset('Assets/angular/ui-bootstrap-tpls-2.5.0.min.js')}}"></script>
    <!--breadcrumb-->
<section class="breadcrumb">
   <div class="container">
      <ol class="cm-breadcrumb">
         <li><a href="#">Home</a></li>
         <li><a href="#">News</a></li>
           @foreach ($data  as $mycity)
         <li class="cm-active">{{$mycity->name}}</li>
          @endforeach

      </ol>
   </div>
</section>
<!--end breadcrumb-->
<!--content-->

<section class="content item-list">
   <div class="container">
      <div class="row">
         <div class="col-md-9 page-content-column">
     
         
            <div class="col-sm-12 col-md-12 col-lg-12 mt-4">
             @foreach ($data as $news)
                <div class="card" style="    font-size: 15px;">
                <div class="col-sm-12 text-center">
                   <h2  style="text-align: center;"> {{$news->name }}</h2>
                   </div>
                   <hr>
                       <div class="col-sm-3 col-md-3 col-lg-3 col-md-offset-1  ">
                    <i class="fa fa-mobile" aria-hidden="true" style="    margin-right: 14px;"></i>{{$news->mobile }}
                    </div>
                                <div class="col-sm-3 col-md-3 col-lg-3 ">
                   <i class="fa fa-phone" aria-hidden="true" style="margin-right: 10px"></i>{{$news->land_line}}
                    </div>
                      <div class="col-sm-4 col-md-4 col-lg-4 ">
                   <i class="fa fa-envelope" aria-hidden="true"></i>{{$news->email}}
                    </div>     
                    
      
                                
                    
                 
           
            <div class="col-sm-10 col-md-10 col-lg-10 col-md-offset-1 " style=" margin-top: 20px;">

            <?php 
                                    echo "{$news->address}";
                                  
                                   ?>
             
                    </div>
                    </div>
                    
                   
              @endforeach
           

             

            


       
        </div>
        </div>

@include('Frontend.sidebar.sidebar')
              
           </div>
       </div>
</section>
    <!-- content end-->
   @endsection