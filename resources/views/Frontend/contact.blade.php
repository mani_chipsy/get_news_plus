 @extends('Frontend.layouts.master_layout')
@section('content')
    <!--content-->
    <section class="content item-list">
      <div class="container">
        <div class="row">
          <div class="col-md-12 page-content-column">
            <h4 class="page-title">Get in touch</h4>
            <div class="row">
            
              <div class="col-sm-8 col-md-8">
                <div class="contact-details contact">
                  <form id="contact1" name="contact1" >
                    <div class="form-group">
                      <input type="text" name="name" class="form-control" placeholder=" Name" required="true">
                    </div>
                    <div class="form-group">
                      <input type="email" name="email"class="form-control" placeholder="Email" required="true">
                    </div>
                    <div class="form-group">
                      <textarea class="form-control" id="message" name="message" placeholder="Message" rows="5" required="true"></textarea>
                    </div>
                    <div class="form-group">
                    <input  style="border: none;width: 20%;
    height: 50px;" type="button" id="mainCaptcha"/>
              <a href="javascript:void(0);" id="refresh" onclick="Captcha();" >
              <i class="fa fa-refresh"  style="font-size: 25px;
}" " aria-hidden="true"></i></a>
                    </div>
                    <div class="form-group">
                   <!--  <input type="text" id="captcha"  name="captcha" required="true"> -->
                    <input type="text" id="txtInput" name="captcha" required="true"/>  
                        </div>
                          <div class="form-group">
             <!--  <input id="Button1" type="button" value="Check" onclick="alert(ValidCaptcha());"/> -->
                      <button type="button" name="btnsend" id="btnsend"  class="btn btn-red btn-sm">Send Message</button>
                    </div>
                    
                  </form>
                </div>
              </div>
              <div class="col-sm-4 col-md-4 contact-details">
                <!-- <p>Maecenas mauris elementum, est morbi at elite imperdiet libero  mauris elementum elite imperdiet libero.</p> -->
                <h2>Get NEWS Plus</h2>
                
                <p>Kelapete Hebri <br>Karkala T. V, Udupi</p>
                <p><strong>Phone: </strong>+9880774413</p>
              <!--   <p><strong>Fax: </strong>+01 34534 5959</p> -->
                <p><strong>Email:</strong><a href="#">getnews0817@gmail.com</a></p>
              </div>
            </div>
          </div>
        </div>
        <div style="height: 40px;"></div>
      </div>
    </section>
    <!-- end content-->
  
@endsection